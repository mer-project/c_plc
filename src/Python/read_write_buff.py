#!/usr/bin/env python3
import sys
import time
from CITL_LinuxPy import *

# Buffer Memory Access
def main():
    # Init CITL Function
    CITLAppInit()
    ulTargetAddr = 16284
    usDataBuf_W = [0xFFFF]
    # Write to Buffer Memory
    sRet = CITL_ToBuf(ulTargetAddr, 1, usDataBuf_W, 0)
    if sRet != 0:
        print("CITL_ToBuf Failed({})\n".format(sRet))
        return -1
    # Read from Buffer Memory
    usDataBuf_R = []
    sRet = CITL_FromBuf(ulTargetAddr, 1, usDataBuf_R, 1)
    if sRet != 0:
        print("CITL_FromBuf Failed({})\n".format(sRet))
        return -1

    print("Buffer data = {}\n".format(usDataBuf_R))


if __name__ == "__main__":
    main()
