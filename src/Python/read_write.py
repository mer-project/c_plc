#!/usr/bin/pyhon3.9

"""export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/citl"""

import sys
from random import randint
from CITL_LinuxPy import *

# Канал для работы
COMM_CHANNEL = 12
# Таймаут подключение, мс
COMM_TIMEOUT = 100
# Интерффейс работы с ПЛК (4-внутренняя шина)
ROUTE = 4
# Номер сети
NET_NUMBER = 0
# Номер модуля
MODULE_NUMBER = 0
# Номер станция
ST_NUMBER = 0
# Номер ЦПУ ПЛК
CPU_NUMBER = 0
# Режим, всегда -1
MODE = -1
# Типы перемнных, для примера взято три
# полный список в иснтрукции Programming Manual
DEV = {"DevD": 13, "DevSD": 14, "DevW": 24}


# Перемнная, хранит путь работы
path = list()


def write_value(dev_type: int, size: list, device: int, data: list) -> int:
    """
    Функция записывает значение указанное в data
    для переменной типа dev_type с адресоом device, размером size (8 или 16 бит) в
    """
    global path
    ret = int
    ret = mdrSend(
        path[0],
        ROUTE,
        NET_NUMBER,
        MODULE_NUMBER,
        ST_NUMBER,
        CPU_NUMBER,
        dev_type,
        device,
        size,
        data,
    )
    if ret == -1:
        return -1
    else:
        return ret


def read_value(dev_type: int, size: list, device: int, data: list) -> int:
    """
    Функция считывает значение и записывает в data
    для переменной типа dev_type с адресоом device, размером size (8 или 16 бит) в
    """
    global path
    ret = int
    ret = mdrReceive(
        path[0],
        ROUTE,
        NET_NUMBER,
        MODULE_NUMBER,
        ST_NUMBER,
        CPU_NUMBER,
        dev_type,
        device,
        size,
        data,
    )
    if ret == -1:
        return -1
    else:
        return ret


if __name__ == "__main__":
    # Пишем слово, рамзмером 2 байта
    size = [2]
    # D12 Адрес пременой для записи
    dev_type = "DevD"
    dev_address = 12
    # Случайное значение для записи
    value = list()
    value.append(randint(1, 4255))
    rt_value = list()
    # Иницилизация библеотеки
    mdrAppInit()
    # Открываем путь для работы
    ret = mdrOpen(COMM_CHANNEL, MODE, path, COMM_TIMEOUT)
    if ret == -1:
        sys.exit("failed open channel", ret)
    if not DEV.get(dev_type):
        print("Type device not found")
    else:
        wr_funct = write_value(DEV.get(dev_type), size, dev_address, value)
        rd_function = read_value(DEV.get(dev_type), size, dev_address, rt_value)
    if rd_function != -1:
        print("D12=", rt_value[0])
        ret = mdrClose(path[0])
        if ret == -1:
            print("failed close connection")
        else:
            print("Connection close")
