"""export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/citl"""

import sys
import time
from random import randint
from CITL_LinuxPy import *

COMM_CHANNEL = 12
COMM_TIMEOUT = 100
# bus interface
ROUTE = 4
# Network Number
NET_NUMBER = 0
# Module Number
MODULE_NUMBER = 0
# Station nmber
ST_NUMBER = 0
# CPU number
CPU_NUMBER = 0
# Device type
DEV = {"DevD": 13, "DevSD": 14, "DevW": 24}
# Specify mode always -1
MODE = -1

path = list()


def write_value(dev_type: str, size: list, device: int, data: list) -> int:
    global path
    ret = int
    if not DEV.get(dev_type):
        return -2
    ret = mdrSend(
        path[0],
        ROUTE,
        NET_NUMBER,
        MODULE_NUMBER,
        ST_NUMBER,
        CPU_NUMBER,
        DEV.get(dev_type),
        device,
        size,
        data,
    )
    if ret == -1:
        return -1
    else:
        return ret


def read_value(dev_type: str, size: list, device: int, data: list) -> int:
    global path
    ret = int
    if not DEV.get(dev_type):
        return -2
    ret = mdrReceive(
        path[0],
        ROUTE,
        NET_NUMBER,
        MODULE_NUMBER,
        ST_NUMBER,
        CPU_NUMBER,
        DEV.get(dev_type),
        device,
        size,
        data,
    )
    if ret == -1:
        return -1
    else:
        return ret


def read_plc_type():
    pass


mdrAppInit()
ret = mdrOpen(COMM_CHANNEL, MODE, path, COMM_TIMEOUT)
if ret == -1:
    sys.exit("failed open channel", ret)

start_time = time.time()
if __name__ == "__main__":
    # size 2 bytes
    for i in range(0, 100):
        size = [2]
        # D12
        dev_address = 12
        # value
        value = list()
        value.append(randint(1, 4255))
        rt_value = list()
        wr_funct = write_value("DevD", size, dev_address, value)
        data_R = list()
        rd_function = read_value("DevD", size, dev_address, rt_value)
        if rd_function != -1:
            print("D12=", rt_value[0])
print("---running programm %s seconds ---" % (time.time() - start_time))
