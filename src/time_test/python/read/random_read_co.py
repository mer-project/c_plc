#read static value
#read 200 Device 
#from D100 to D300
#Batch read
#With close Connection

import time
import sys
from random import randint
from CITL_LinuxPy import *

COMM_CHANNEL = 12
COMM_TIMEOUT = 100
ROUTE = 4
NET_NUMBER = 0
MODULE_NUMBER = 0
ST_NUMBER = 0
CPU_NUMBER = 0
MODE = -1
DEV = {"DevD": 13, "DevSD": 14, "DevW": 24}

path = list()

def read_value(dev_type: int, size: list, device: int, data: list) -> int:
    """
    Функция считывает значение и записывает в data
    для переменной типа dev_type с адресоом device, размером size (8 или 16 бит) в
    """
    global path
    ret = int
    ret = mdrReceive(
        path[0],
        ROUTE,
        NET_NUMBER,
        MODULE_NUMBER,
        ST_NUMBER,
        CPU_NUMBER,
        dev_type,
        device,
        size,
        data,
    )
    if ret == -1:
        return -1
    else:
        return ret


if __name__ == "__main__":
    time_arr=[]
    size = [2]
    dev_type = "DevD"
    rt_value = list()
    mdrAppInit()
    j=0
    while j <=20:
        start_time = time.time()
        ret = mdrOpen(COMM_CHANNEL, MODE, path, COMM_TIMEOUT)
        if ret == -1:
            sys.exit("failed open channel", ret)
        if not DEV.get(dev_type):
            print("Type device not found")
        print("Reading.....")
        i = randint(100,300)
        rd_function = read_value(DEV.get(dev_type), size, int(i), rt_value)
        ret = mdrClose(path[0])
        if ret == -1:
            print("failed close connection")
        else:
           print("Connection close")
        time_arr.append(time.time() - start_time)
        j += 1
    print(time_arr)
    with open ("/root/c_plc/src/time_test/randrom_read_co.txt",'a') as f:
        f.write("static text\n")
        f.write(f"time = {time_arr}\n")
