import sys
import time
from random import randint
from CITL_LinuxPy import *

# Канал для работы
COMM_CHANNEL = 12
# Таймаут подключение, мс
COMM_TIMEOUT = 100
# Интерффейс работы с ПЛК (4-внутренняя шина)
ROUTE = 4
# Номер сети
NET_NUMBER = 0
# Номер модуля
MODULE_NUMBER = 0
# Номер станция
ST_NUMBER = 0
# Номер ЦПУ ПЛК
CPU_NUMBER = 0
# Режим, всегда -1
MODE = -1
# Типы перемнных, для примера взято три
# полный список в иснтрукции Programming Manual
DEV = {"DevD": 13, "DevSD": 14, "DevW": 24}


# Перемнная, хранит путь работы
path = list()

def write_value(dev_type: int, size: list, device: int, data: list) -> int:
    """
    Функция записывает значение указанное в data
    для переменной типа dev_type с адресоом device, размером size (8 или 16 бит) в
    """
    global path
    ret = int
    ret = mdrSend(
        path[0],
        ROUTE,
        NET_NUMBER,
        MODULE_NUMBER,
        ST_NUMBER,
        CPU_NUMBER,
        dev_type,
        device,
        size,
        data
    )
    if ret == -1:
        return -1
    else:
        return ret


if __name__ == "__main__":
    time_arr=[]
    size = [2]
    
    dev_type = "DevD"
    mdrAppInit()
    ret = mdrOpen(COMM_CHANNEL, MODE, path, COMM_TIMEOUT)
    if ret == -1:
        sys.exit("failed open channel", ret)
    if not DEV.get(dev_type):
        print("Type device not found")
    j = 0
    print("Reading.....")
    while j <=20:
        value = list()
        start_time = time.time()
        i = randint(500,700)
        value.append(randint(0,32001))
        wr_funct = write_value(DEV.get(dev_type), size, int(i),value)
        time_arr.append(time.time() - start_time)
        j += 1
    ret = mdrClose(path[0])
    if ret == -1:
        print("failed close connection")
    else:
        print("Connection close")
    print(time_arr)
    with open ("/root/c_plc/src/time_test/write/random_write_time.txt",'a') as f:
        f.write("static text\n")
        f.write(f"time = {time_arr}\n")