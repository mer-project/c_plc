#include <iostream>
#include <time.h>

#include "MDRFunc.h"

//Канал для работы
#define COMM_CHANNEL  12
//Таймаут подключение, мс
#define COMM_TIMEOUT  100
//Интерффейс работы с ПЛК (4-внутренняя шина)
#define ROUTE  4
//Номер сети
#define NET_NUMBER  0
//Номер модуля
#define MODULE_NUMBER  0
//Номер станция
#define ST_NUMBER  0
//Номер ЦПУ ПЛК
#define  CPU_NUMBER  0
//Режим, всегда -1
#define MODE -1

int main(void){
	short sRet = 0;
	long lPath = 0;
	long lSize = 2; 
	short sData_R = 0;	
	short sDevAdrr = 12;
	float diff;
	time_t start_time;
	time_t current_time;

	mdrAppInit();
	

	sRet=mdrOpen(COMM_CHANNEL,MODE,&lPath,COMM_TIMEOUT);
	if (sRet !=0){
		std::cout<<"mdrOpen failed " << sRet<<std::endl;
		return -1;
	}

	time(&start_time);
	for (int i=100; i<=300; i++){
		sDevAdrr=i;
		sRet=mdrReceive(lPath, ROUTE, NET_NUMBER, MODULE_NUMBER, ST_NUMBER, CPU_NUMBER, DevD, sDevAdrr, &lSize, &sData_R);

	}
	time(&current_time);
	diff = difftime(current_time, start_time)/1000;
	printf("time=%f\n",diff);

	sRet = mdrClose(lPath);
	if (sRet !=0){
	       std::cout<<"mdrClose failed " << sRet<<std::endl;
       	    return -1;
	}		       
	
	return 0;
}

