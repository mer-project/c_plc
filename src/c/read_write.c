#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "MDRFunc.h"

//Канал для работы
#define COMM_CHANNEL  12
//Таймаут подключение, мс
#define COMM_TIMEOUT  100
//Интерффейс работы с ПЛК (4-внутренняя шина)
#define ROUTE  4
//Номер сети
#define NET_NUMBER  0
//Номер модуля
#define MODULE_NUMBER  0
//Номер станция
#define ST_NUMBER  0
//Номер ЦПУ ПЛК
#define  CPU_NUMBER  0
//Режим, всегда -1
#define MODE -1

int main(void){
	short sRet = 0;
	long lPath = 0;
	long lSize = 2; 
	short sData_W = 0;
	short sData_R = 0;	
	short sDevAdrr = 12;

	mdrAppInit();
	srand(time(NULL));

	sRet=mdrOpen(COMM_CHANNEL,MODE,&lPath,COMM_TIMEOUT);
	if (sRet !=0){
		printf("mdrOpen failed %d\n", sRet);
		return -1;
	}

	sData_W=rand()%100 + 32;

	sRet=mdrSend(lPath,ROUTE,NET_NUMBER,MODULE_NUMBER,ST_NUMBER,CPU_NUMBER,DevD,sDevAdrr,&lSize,&sData_W);
	if (sRet != 0) {
		printf("mdrReceive failed %d\n", sRet);
		return -1;
	}

	sRet=mdrReceive(lPath, ROUTE, NET_NUMBER, MODULE_NUMBER, ST_NUMBER, CPU_NUMBER, DevD, sDevAdrr, &lSize, &sData_R);
	if (sRet != 0) {
		printf("mdrReceive failed %d\n", sRet);
		return -1;
	}

	printf("D12=%d\n",sData_R);

	sRet = mdrClose(lPath);
	if (sRet !=0){
	       printf("mdrClose failed %d\n", sRet);
       	       return -1;
	}		       
	
	return 0;
}

