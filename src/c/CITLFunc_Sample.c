/*
 *
 * CITLFunc_Sample.c
 *
 * Copyright (C) 2020  Lineo Solutions, Inc.
 *
 * */

#include <stdio.h>
#include "CITLFunc.h"


#define TARGET_ADDR	16384

/* Buffer Memory Access */
int main(void){
	short sRet = 0;
	unsigned short usDataBuf_W = 0;	/* Write Data */
	unsigned short usDataBuf_R = 0;	/* Read Data */
        printf("aa!!");
	CITLAppInit();
        printf("qweqw");	
	/* Write to Buffer Memory */
	usDataBuf_W = 0xFFFF;
	sRet = CITL_ToBuf(TARGET_ADDR, 1, &usDataBuf_W, 0);
	if(sRet != 0){
		printf("CITL_ToBuf Failed(%d)\n", sRet);
		return -1;
	}
	
	/* Read from Buffer Memory */
	sRet = CITL_FromBuf(TARGET_ADDR, 1, &usDataBuf_R, 1);
	if(sRet != 0){
		printf("CITL_FromBuf Failed(%d)\n", sRet);
		return -1;
	}
	
	printf("Buffer data = %d\n", usDataBuf_R);
	
	return 0;
}
