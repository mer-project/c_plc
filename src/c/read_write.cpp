#include <iostream>
#include <ctime>
#include "MDRFunc.h"

//Канал для работы
#define COMM_CHANNEL  12
//Таймаут подключение, мс
#define COMM_TIMEOUT  100
//Интерффейс работы с ПЛК (4-внутренняя шина)
#define ROUTE  4
//Номер сети
#define NET_NUMBER  0
//Номер модуля
#define MODULE_NUMBER  0
//Номер станция
#define ST_NUMBER  0
//Номер ЦПУ ПЛК
#define  CPU_NUMBER  0
//Режим, всегда -1
#define MODE -1

int main(void){
	short sRet = 0;
	long lPath = 0;
	long lSize = 2; 
	short sData_W = 0;
	short sData_R = 0;	
	short sDevAdrr = 12;

	mdrAppInit();
	
    std::srand(std::time(nullptr));

	sRet=mdrOpen(COMM_CHANNEL,MODE,&lPath,COMM_TIMEOUT);
	if (sRet !=0){
		std::cout<<"mdrOpen failed " << sRet<<std::endl;
		return -1;
	}

	sData_W=std::rand()%100 + 32;

	sRet=mdrSend(lPath,ROUTE,NET_NUMBER,MODULE_NUMBER,ST_NUMBER,CPU_NUMBER,DevD,sDevAdrr,&lSize,&sData_W);
	if (sRet != 0) {
		std::cout<<"mdrSend failed " << sRet<<std::endl;
		return -1;
	}

	sRet=mdrReceive(lPath, ROUTE, NET_NUMBER, MODULE_NUMBER, ST_NUMBER, CPU_NUMBER, DevD, sDevAdrr, &lSize, &sData_R);
	if (sRet != 0) {
		std::cout<<"mdrReceive failed " << sRet<<std::endl;
		return -1;
	}

	std::cout<<"D12="<<sData_R<<std::endl;

	sRet = mdrClose(lPath);
	if (sRet !=0){
	       std::cout<<"mdrClose failed " << sRet<<std::endl;
       	    return -1;
	}		       
	
	return 0;
}

