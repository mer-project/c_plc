#include <stdio.h>
#include "MDRFunc.h"

#define COMM_CH 12
#define COMM_TIMEOUT 10

int main(void){
	short sRet = 0;
	long lPath = 0;
	long lSize = 4; 
	short sData_R = 0;
	short sPSCode =0;	

	mdrAppInit();

	sRet=mdrOpen(COMM_CH,-1,&lPath,COMM_TIMEOUT);
	if (sRet !=0){
		printf("mdrOpen failed %d\n", sRet);
		return -1;
	}
	sRet = mdrReceive(lPath, 4, 0, 0, 0, 0, DevD, 0, &lSize, &sData_R);
	if (sRet != 0) {
		printf("mdrReceive failed %d\n", sRet);
		mdrClose(lPath);
		return -1;
	}

	printf("D0=%d\n",sData_R);

	sRet = mdrTypeRead(lPath,4,0,0,0,0, &sPSCode);
	if (sRet != 0){
		printf("mdrTypeRead failed %d\n", sRet);
		return -1;
	}

	printf("PlC Type - 0x%04hXH\n" , sPSCode);

	sRet = mdrClose(lPath);
	if (sRet !=0){
	       printf("mdrClose failed %d\n", sRet);
       	       return -1;
	}		       
	

	return 0;

}

