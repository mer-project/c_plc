#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include "CITLFunc.h"
#include "MDRFunc.h"

#define COMM_CH			12
#define COMM_TIMEOUT	360

int main()
{
	short sRet = 0, type = 0;
	long lPath = -1;

	sRet = CITLAppInit();
	sRet = mdrAppInit();
	printf("mdrAppInit -> (%i)\n", sRet);
	
	sRet = mdrOpen(COMM_CH, -1, &lPath, COMM_TIMEOUT);
	if(sRet)
	{
		printf("Can't open channel (%i)\n", sRet);
		return -1;
	}
	printf("Communication OK\n");
	
	sRet = mdrTypeRead(lPath, 4, 0, 0, 0, 0, &type);
	printf("Read type %x\n", type);

	sRet = mdrClose(lPath);
	if(sRet)
	{
		printf("Can't close channel (%i)\n", sRet);
		return -1;
	}

	return 0;
}

